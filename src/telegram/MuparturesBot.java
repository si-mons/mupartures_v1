package telegram;

import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.objects.Ability;

import java.util.List;

import gateway.*;

import static org.telegram.abilitybots.api.objects.Locality.ALL;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;;

public class MuparturesBot extends AbilityBot {

	public static final String BOT_TOKEN = "547957397:AAGC2bCgXBTMxbNnlmiIi43N5x2WLBLQVOg";
	public static final String BOT_USERNAME = "muparturesbot";
	private static final int BOT_CREATORID = 317987483;

	private MvgGateway gateway;

	public MuparturesBot() {
		super(BOT_TOKEN, BOT_USERNAME);
		gateway = new MvgGateway();
	}

	@Override
	public int creatorId() {
		return BOT_CREATORID;
	}

	public Ability start() {
		return Ability.builder().name("start").locality(ALL).privacy(PUBLIC).input(0).action(ctx -> {
			silent.send(Phrase.START_TEXT.toString().replace("%name%", ctx.user().firstName()), ctx.chatId());
		}).build();
	}

	public Ability defaultAbility() {
		return Ability.builder().name(DEFAULT).privacy(PUBLIC).locality(ALL)
				.input(0).action(ctx -> {
					// string builder for answer
					StringBuilder stringBuilder;

					// get arguments
					String[] args = ctx.arguments();

					// check arguments length
					if (args.length == 0 || args.length > 2) {
						// send help phrase
						silent.send(Phrase.HELP.toString(), ctx.chatId());
					} else {
						// check station
						String name = ctx.firstArg().replaceAll("-", "+");

						// happy eastern
						if (name.toLowerCase().equals("donnersburk")) {
							silent.send("play hard burk hard", ctx.chatId());

							return;
						}

						List <Location> stations = gateway.getStationsByName(name);
						if (stations.isEmpty()) {
							// send empty text
							silent.send(Phrase.NO_STATIONS.toString(), ctx.chatId());

							return;

						} else if (stations.size() > 1 &&
								!stations.get(0).getName().replaceAll(" ", "+").equals(name)) {
							// print all stations to reenter command (max 30)
							stringBuilder = new StringBuilder(name + "?\n");
							for (int i = 0; i < 30 && i < stations.size(); i++) {
								stringBuilder.append(stations.get(i).getName().replaceAll(" ", "-")+"\n");
							}

							// check if there would be more
							if (stations.size() > 30) {
								stringBuilder.append("...");
							}

							// send list of possible stations
							silent.send(stringBuilder.toString(), ctx.chatId());

							return;
						}

						// check product if present
						List<Departure> departures;
						String product = null;
						if (args.length == 2) {
							product = ctx.secondArg().toUpperCase().substring(0, 1);
							if (! Phrase.PRODUCTS_SHORT.toString().contains(product)) {
								// send all possible products
								silent.sendMd(ctx.secondArg() + "?\n" + Phrase.PRODUCTS_LONG, ctx.chatId());
								
								return;
							}
							
							// get departures with product filter
							departures = gateway.getDeparturesByStationId(
									stations.get(0).getId(), product);
						} else {
							// get all departures at station
							departures = gateway.getDeparturesByStationId(stations.get(0).getId());
						}

						// check departures
						if (departures.isEmpty()) {
							// send empty text
							silent.send(Phrase.NO_DEPARTURES.toString(), ctx.chatId());
						} else {
							// print departures
							stringBuilder = new StringBuilder(String.format("*%s* %s\n\n", 
									stations.get(0).getName(), product != null ? product : ""));
							for (Departure departure : departures) {
								stringBuilder.append(departure);
							}

							// send message
							silent.sendMd(stringBuilder.toString(), ctx.chatId());
						}
					}
				}).build();
	}
}