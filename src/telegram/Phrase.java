package telegram;

public enum Phrase {
	START_TEXT("Na %name% - kein Ticket kein Problem?\nSchreib mir einfach die Station (+ optional. Transportmittel)"),
	HELP("<Station> <Art?>"),
	PRODUCTS_SHORT("SUTB"),
	PRODUCTS_LONG("*S*bahn/*U*bahn/*T*ram/*B*us"),
	NO_DEPARTURES("keine Abfahrten - geh zu Fuß"),
	NO_STATIONS("keine Station gefunden");

	private final String text;

	private Phrase(final String text) {
		this.text = text;
	}

	public String toString() {
		return text;
	}
}
