package gateway;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MvgGateway {

	private String apiUrl = "https://www.mvg.de/";
	private String apiKey = "5af1beca494712ed38d313714d4caff6";

	private String queryUrlName = "fahrinfo/api/location/queryWeb?q=%s";
	private String departureUrl = "fahrinfo/api/departure/%s?footway=0";

	private PoolingHttpClientConnectionManager connManager;
	private CloseableHttpClient client;

	private static Type locationsType = new TypeToken<Map<String, List<Location>>>() {}.getType();
	private static Type departuresType = new TypeToken<Map<String, List<Departure>>>() {}.getType();

	public MvgGateway() {
		this.connManager = new PoolingHttpClientConnectionManager();
		int maxConns = 40;
		connManager.setMaxTotal(maxConns);
		connManager.setDefaultMaxPerRoute(maxConns);
		HttpHost host = new HttpHost(apiUrl, 8080);
		connManager.setMaxPerRoute(new HttpRoute(host), maxConns);

		BasicHeader header = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
		List<BasicHeader> headers = new ArrayList<BasicHeader>(1);
		headers.add(header);
		this.client = HttpClients.custom().setDefaultHeaders(headers).setConnectionManager(connManager).build();
	}

	public List<Location> getStationsByName(String name){
		HttpGet httpGet = null;
		// create list for filtered locations
		List<Location> resultList = new ArrayList<Location>();

		try {
			// build request
			httpGet = new HttpGet(apiUrl + String.format(queryUrlName, name));
			httpGet.setHeader("X-MVG-Authorization-Key", apiKey);
			httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");

			// do request
			CloseableHttpResponse response = client.execute(httpGet);

			// check request
			if (response.getStatusLine().getStatusCode() == 200) {

				// create reader
				InputStreamReader responseReader = new InputStreamReader(response.getEntity().getContent());
				// create map from json data
				Map<String, List<Location>> data = new Gson().fromJson(responseReader, locationsType);
				// filter out all stations
				List<Location> locations = data.get("locations");
				for (Location l : locations) {
					if (l.getType() != null && l.getType().equals("station")) {
						// add station to result list
						resultList.add(l);
					}
				}

				// close response
				response.close();

			} else {
				// print status line
				System.err.println(response.getStatusLine());
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			httpGet.releaseConnection();
		}

		return resultList;
	}

	public List<Departure> getDeparturesByStationId(String id) {
		return this.getDeparturesByStationId(id, null);
	}

	public List<Departure> getDeparturesByStationId(String id, String product) {
		// build request
		HttpGet httpGet = new HttpGet(apiUrl + String.format(departureUrl, id));
		httpGet.setHeader("X-MVG-Authorization-Key", apiKey);
		httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");

		// create list for filtered departures
		List<Departure> resultList = new ArrayList<Departure>();

		try {
			// do request
			CloseableHttpResponse response = client.execute(httpGet);

			// check request
			if (response.getStatusLine().getStatusCode() == 200) {

				// create reader
				InputStreamReader responseReader = new InputStreamReader(response.getEntity().getContent());
				// create map from json data
				Map<String, List<Departure>> data = new Gson().fromJson(responseReader, departuresType);

				// filter/limit to 15 entries
				for (Departure departure : data.get("departures")) {
					if (resultList.size() < 10 &&
							(product == null || product.length() == 0 ||
							departure.getProduct().toUpperCase().substring(0, 1).equals(product))) {
						// add
						resultList.add(departure);
					}
				}

				// close response
				response.close();

			} else {
				// print status line
				System.err.println(response.getStatusLine());
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			httpGet.releaseConnection();
		}

		return resultList;
	}
}
