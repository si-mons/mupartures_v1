package gateway;

public class Location {

	private String id;
	private String type;
	private String name;

	public Location(String id, String type, String name) {
		this.id = id;
		this.type = type;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
