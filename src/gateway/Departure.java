package gateway;

public class Departure {
	private String product;
	private String label;
	private String destination;
	private long departureTime;

	public Departure(String product, String label, String destination, long departureTime) {
		this.product = product;
		this.label = label;
		this.destination = destination;
		this.departureTime = departureTime;
	}

	public String getProduct() {
		return product;
	}

	public String getLabel() {
		return label;
	}

	public String getDestination() {
		return destination;
	}

	public long getDepartureTime() {
		return departureTime;
	}

	public long getMinutesTillDept() {
		return (this.getDepartureTime() - System.currentTimeMillis()) / 60000;
	}

	@Override
	public String toString() {
		return String.format("%.20s *%smin*\n", this.getLabel() + " " + this.getDestination(), this.getMinutesTillDept());
	}
}
